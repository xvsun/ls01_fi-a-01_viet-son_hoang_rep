﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       //Scanner tastatur = new Scanner(System.in);
    	boolean wiederholung = true; //für die Schleife
    	double zuZahlenderBetrag; //Datentyp double ; Variable
    	double eingezahlterGesamtbetrag; //Datentyp double ; Variable
       
       while (wiederholung==true) { //solange Wiederholung = true -> Wiederholung
    	   	zuZahlenderBetrag = fahrkartenbestellungErfassen();
           
    	   	eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

    	   	fahrkartenAusgeben();
    	      
    	   	rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
       }
      
  
    }
	static double fahrkartenbestellungErfassen() {
		double auswahl; //Auswahl für Ticketart
		double ticket;
		double zuZahlenderBetrag;
		double ticket1= 3.00; //Kosten für AB Ticket
		double ticket2= 8.80 ; // Kosten für Tageskarte AB 
		double ticket3= 25.50 ; // Kosten für Kleingruppe AB
		
	    Scanner taste = new Scanner(System.in);
	    System.out.println("\n-----------Bestellvorgang-------------------");
    	System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\r\n" + 
    			"  Einzelfahrschein Regeltarif AB [3,00 EUR] (1)\n" + 
    			"  Tageskarte Regeltarif AB [8,60 EUR] (2)\n" + 
    			"  Kleingruppen-Tageskarte Regeltarif AB [25,50 EUR] (3)");
        auswahl = taste.nextDouble(); //Eingabe Ticketauswahl
        if (auswahl==1) { //wenn Ticketart 1 ausgewählt dann wird AB Ticket Preis genommen
        	auswahl = ticket1;
        }
        else if(auswahl == 2) {
        	auswahl= ticket2;
        }
        else if (auswahl == 3) {
        	auswahl = ticket3;
        }
        System.out.print("Anzahl der Tickets (mind. 1 und max. 10): ");
        ticket = taste.nextDouble(); // Eingabe Tickets
        
        if (ticket >= 1 && ticket <= 10) { //wenn Ticket Eingabe zwischen 1 und 10
        	 zuZahlenderBetrag = auswahl * ticket;
        	 System.out.println("Zu zahlender Betrag: " + zuZahlenderBetrag);
        	 
        }
        else { // bei ungültiger Eingabe
        	System.out.println("Eingabe (" + ticket + " )ungültig - Ticket Zahl wird auf Standard 1 gesetzt ");
        	ticket = 1;
        	zuZahlenderBetrag = auswahl * ticket;
       	 	System.out.println("Zu zahlender Betrag: " + zuZahlenderBetrag);
        }
        
        return (zuZahlenderBetrag); //Rückgabewert zuZahlenderBetrag
    }
	
    static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	 // Geldeinwurf
    	Scanner taste = new Scanner(System.in);
    	double eingeworfeneMünze;
        double eingezahlterGesamtbetrag = 0.0;
        double nochZahlen = zuZahlenderBetrag;
        
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag)	{
        	
        
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n",nochZahlen);
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze= taste.nextDouble();
     	   nochZahlen = nochZahlen - eingeworfeneMünze;
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           System.out.printf("Eingezahlt: %.2f Euro\n",eingezahlterGesamtbetrag);
           
           }
    	return (eingezahlterGesamtbetrag);	}
    
    static void fahrkartenAusgeben() {
    	
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	return; 
    }
    
    static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	// Rückgeldberechnung und -Ausgabe
        // -------------------------------
        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
 	          rückgabebetrag = Math.round(100 * rückgabebetrag) / 100.00; //aufrunden des Betrags
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
 	          rückgabebetrag = Math.round(100 * rückgabebetrag) / 100.00;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
 	          rückgabebetrag = Math.round(100 * rückgabebetrag) / 100.00;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
  	         rückgabebetrag = Math.round(100 * rückgabebetrag) / 100.00;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
 	          rückgabebetrag = Math.round(100 * rückgabebetrag) / 100.00;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
  	          rückgabebetrag = Math.round(100 * rückgabebetrag) / 100.00;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
}