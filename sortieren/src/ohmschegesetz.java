import java.util.Scanner;
public class ohmschegesetz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char R,U,I;
		double v, a, ohm;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Dieses Programm verwendet die Formel des Ohmsche Gesetzes.");
		System.out.println("Bitte geben sie erst an welche Größe (R / U / I) berechnet werden soll:");
		char eingabe = scan.next().charAt(0);
		
		switch(eingabe) {
		
		case 'R':
			System.out.println("Geben sie die Spannung U in Volt an: ");
			
					v = scan.nextInt();
				
			System.out.println("Geben sie die Stromstärke in Ampere an: ");
			
					a = scan.nextInt();
				
			scan.close();
			
					ohm = v / a;
				
			System.out.println("Der Widerstand mit einer Spannung von "+ v + "V " + "und einer Stromstärke von " + a + "A" + " beträgt " + ohm + "Ω");
			
			break;
			
			
		case 'U':
			System.out.println("Geben sie die Stromstärke in A an: ");
			
				a = scan.nextInt();
			
		System.out.println("Geben sie den Widerstand in Ohm an: ");
		
				ohm = scan.nextInt();
			
		scan.close();
		
				v = ohm / a;
			
		System.out.println("Die Spannung mit einem Widerstand von "+ ohm + "Ω" + " und einer Stromstärke von " + a + "A" + " beträgt " + v + " V");
			
		
			break;
		case 'I':
			
System.out.println("Geben sie die Spannung in V an: ");
			
				v = scan.nextInt();
			
		System.out.println("Geben sie den Widerstand in Ohm an: ");
		
				ohm = scan.nextInt();
			
		scan.close();
		
				a = v / ohm;
			
		System.out.println("Die Stromstärke mit einem Widerstand von "+ ohm + "Ω" + " und einer Spannung von " + v + "V" + " beträgt " + a + " A");
			
			
			break;
		default:
			System.out.println("Eingabe ist ungültig. Bitte starten Sie das Programm neu.");
			
			break;
		}
	}

}
