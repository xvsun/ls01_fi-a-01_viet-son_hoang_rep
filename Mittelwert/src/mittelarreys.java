import java.util.Scanner;
import java.util.Random;

public class mittelarreys{

   public static void main(String[] args) {
	   
     double m;
	
      // Eingabe
      int arraylang = arraylenght();
      
	   // generate Zufallsnummern
	   double[] array = generateNumbers(arraylang);
      
      
      
      // (V) Verarbeitung

       m = berechneMittelwert(array, arraylang);
      
      // (A) Ausgabe

      ausgabe (arraylang,array, m);
            
   }
   //Array mit zuf�llig generierten Nummern
   public static int arraylenght () {
	   Scanner scan = new Scanner(System.in);
	    
	   System.out.println("Gib die L�nge des Arrays ein: ");
	   int arraylang = scan.nextInt();
	   return arraylang;
   }
   
   public static double[] generateNumbers(int lang) {
	   double array[] = new double[lang];
	   int rdm;
	   for (int i=0; i < lang; i++) {
		   
		   rdm = (int)(Math.random() * 99);
		   array[i] = rdm;
	   }
	   return array; //R�ckgabe von array
   }
   
   
   // Methode Verarbeitung
   public static double berechneMittelwert(double[] array, int arraylang) {
      
      double mittelwert= 0;
      
      for (int i=0; i < arraylang; i++) {
    	  mittelwert = mittelwert + array[i];
    			     
      }
      mittelwert = mittelwert / arraylang;
      
      return mittelwert;
   }
   
   
   // Methode Ausgabe
   public static void ausgabe (int arraylang, double[] array, double mittelwert) {
	   
	   System.out.println("Der Mittelwert von: ");
	   
	   for (int i=0; i<arraylang; i++) {
		   
		   System.out.printf(array[i]+" ");
	   }
	   System.out.print("ist " + mittelwert + "!");
	   
   }
}
