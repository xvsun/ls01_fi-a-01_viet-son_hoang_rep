import java.util.Scanner;

public class mittel {
	public static void main(String[] args) {
		
		//Werte festlegen
		double wert1;
		double wert2;
		double wert3;
		double ergebnis2, ergebnis3;
		
		Scanner tastatur = new Scanner(System.in); //Tastatur aktivieren
		
		
		programmhinweis();
		
		
		System.out.println("Geben sie den ersten Wert ein: ");
		wert1 =tastatur.nextDouble();
		
		System.out.println("Geben sie den zweiten Wert ein: ");
		wert2 = tastatur.nextDouble();
		
		System.out.println("Geben sie den dritten Wert ein: ");
		wert3 = tastatur.nextDouble();
		
		//Berechnung
		ergebnis2 = mittelwert(wert1,wert2); //Aufruf der Methode und �bergabe der Werte
		ergebnis3 = mittelwert(wert1,wert2,wert3);
		
		//Ausgabe der Ergebnisse
		programmhinweis();
		System.out.printf("Der Mittelwert von %.2f und %.2f lautet: %.2f\n", wert1, wert2, ergebnis2);
		
		System.out.printf("Der Mittelwert von %.2f, %.2f und %.2f lautet: %.2f ", wert1, wert2, wert3, ergebnis3);
		tastatur.close();
	}
	//R�ckgabetyp - Methodenname - Parameterliste
	static		double mittelwert	(double wert1, double wert2) { //Signatur der Methode
//	double ergebnis2=0.0;
//	ergebnis2= (wert1 + wert2) / 2; //Methodenrumpf
//	return ergebnis2;
	return(wert1+wert2)/2;
	
}
static double mittelwert(double wert1, double wert2, double wert3) {
//	double ergebnis3=0.0;
//	ergebnis3= (wert1 + wert2 + wert3) / 3.0;
//	return ergebnis3;
	
	return(wert1 + wert2 + wert3) /3.0;
}

static void programmhinweis() {
	System.out.println("Dieses Programm rechnet den Mittelwert von zwei oder drei Zahlen.");
}

}
